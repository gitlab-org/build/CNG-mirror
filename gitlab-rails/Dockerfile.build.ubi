ARG BUILD_IMAGE=
ARG ASSETS_IMAGE=

FROM ${ASSETS_IMAGE} AS assets
FROM ${BUILD_IMAGE}

ARG NAMESPACE=gitlab-org
ARG PROJECT=gitlab
ARG VERSION=v12.0.2-ee
ARG API_URL=
ARG API_TOKEN=
ARG FIPS_MODE=

ARG NODE_VERSION=20.12.2
ARG RUBY_VERSION=3.2.5
ARG YARN_VERSION=1.22.18
ARG GITLAB_USER=git
ARG ARCH=x64

ADD gitlab-ruby.tar.gz /
ADD gitlab-rust.tar.gz /
ADD gitlab-python.tar.gz /
ADD postgresql.tar.gz /
ADD gitlab-elasticsearch-indexer.tar.gz /
ADD gitlab-graphicsmagick.tar.gz /

ENV PRIVATE_TOKEN=${API_TOKEN}
ENV LIBDIR ${LIBDIR:-"/usr/lib64"}

RUN microdnf ${DNF_OPTS} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 git krb5-devel clang-devel

COPY shared/build-scripts/ /build-scripts

RUN NODE_TARBALL="node-v${NODE_VERSION}-linux-${ARCH}.tar.gz" ; \
    /build-scripts/curl_retry_netfail "https://nodejs.org/download/release/v${NODE_VERSION}/${NODE_TARBALL}" ${NODE_TARBALL} \
    && tar --strip-components 1 -xzC /usr/local/ -f ${NODE_TARBALL}

RUN mkdir /usr/local/yarn ; YARN_TARBALL="yarn-v${YARN_VERSION}.tar.gz" ; \
    /build-scripts/curl_retry_netfail "https://yarnpkg.com/downloads/${YARN_VERSION}/${YARN_TARBALL}" ${YARN_TARBALL} \
    && tar --strip-components 1 -xzC /usr/local/yarn -f ${YARN_TARBALL} \
    && ln -sf /usr/local/yarn/bin/yarn /usr/local/bin/ \
    && ln -sf /usr/local/yarn/bin/yarnpkg /usr/local/bin/

RUN mkdir /assets \
    && install /usr/local/postgresql/bin/* /usr/bin/ \
    && cp -R /usr/local/postgresql/lib/. ${LIBDIR}/ \
    && cp -R /usr/local/postgresql/include/. /usr/include/ \
    && cp -R /usr/local/postgresql/share/. /usr/share/ \
    && printf 'install: --no-document\nupdate: --no-document\n' > ~/.gemrc

ENV RAILS_ENV=production
ENV NODE_ENV=production
ENV USE_DB=false
ENV SKIP_STORAGE_VALIDATION=true

RUN /gitlab-fetch \
        "${API_URL}" \
        "${NAMESPACE}" \
        "${PROJECT}" \
        "${VERSION}" \
    && mkdir -p /srv \
    && mv ${PROJECT}-${VERSION} /srv/gitlab \
    && cp REVISION /srv/gitlab \
    && cd /srv/gitlab \
    && printf 'gitlab-cloud-native-image' > INSTALLATION_TYPE \
    && for cfg in config/{gitlab,resque,secrets}.yml; do cp ${cfg}.example ${cfg} ; done \
    && cp config/database.yml.postgresql config/database.yml \
    && bundle config set --local version system \
    && bundle config set --local path /srv/gitlab/vendor/bundle \
    && bundle config set --local deployment 'true' \
    && bundle config set --local without 'development test mysql aws' \
    && bundle install --jobs $(nproc) --retry 5 \
    && FIPS_MODE=${FIPS_MODE} /build-scripts/reinstall-grpc-if-fips /build-scripts/patches \
    && yarn install --production --pure-lockfile \
    && rm -rf node_modules/ tmp/ spec/ ee/spec/ storybook/ \
    && rm -rf qa/ rubocop/ tooling/ .gitlab/ .github/ docker/ \
    && rm -rf changelogs/ danger/ Dangerfile \
    && RUBY_VERSION=${RUBY_VERSION} /build-scripts/cleanup-gems /srv/gitlab/vendor/bundle/ruby \
    && mkdir /assets/licenses && cp LICENSE /assets/licenses/GitLab.txt

COPY --from=assets assets /srv/gitlab/public/assets/

RUN find /srv/gitlab/public/assets/webpack -name '*.map' -type f -print -delete \
    && mkdir -p /assets/usr/{bin,lib} /assets${LIBDIR} \
    && cp /usr/local/bin/gitlab-elasticsearch-indexer /assets/usr/bin/ \
    && install /usr/local/postgresql/bin/* /assets/usr/bin/ \
    && cp -R /usr/local/postgresql/lib/. /assets${LIBDIR}/ \
    && cp -R --parents \
        /srv/gitlab \
        /usr/local/bin/gm \
        /usr/local/share/doc/GraphicsMagick/Copyright.txt \
        /assets
