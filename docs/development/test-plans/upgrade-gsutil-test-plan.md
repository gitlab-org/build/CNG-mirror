# `gsutil` component upgrade test plan

Steps to validate gsutil basic commands work:

1. Verify that the Pipeline is green.
1. Review the Changelog.
1. Make sure you have a GCP bucket created.
1. Make sure you have a IAM service account, and you have your local keys for it, under "${HOME}/.config/gcloud/legacy_credentials/gcs-adc.json":

   ```
   export GOOGLE_APPLICATION_CREDENTIALS="${HOME}/.config/gcloud/legacy_credentials/gcs-adc.json"

   gcloud iam service-accounts keys create \
     ${GOOGLE_APPLICATION_CREDENTIALS} \
     --iam-account=${IAM_ACCOUNT}
   ```

1. Export these 3 variables in your local terminal:

   ```sh
   # These are just examples
   export BUCKET_URL="gs://my-user-backup-test-storage"
   export PROJECT_ID="foobar-safdn9238"
   export TAG="renovate-googlecloudplatform-gsutil-5-x"
   ```

1. Run the test script `./dev/gsutil/test.sh`, with your docker engine running.
1. Verify that the output had no failures and properly executes `ls`, `copy` and `rm` commands.

   You should see an output similar to:

   ```
   $ ./dev/gsutil/test.sh
   ...
   gsutil version: 5.33
   gs://my-user-backup-test-storage/
   coping file 1234567890_1234_12_12_12_TEST_gitlab_backup.tar to backup bucket gs://my-user-backup-test-storage/1234567890_1234_12_12_12_TEST_gitlab_backup.tar
   Copying file://1234567890_1234_12_12_12_TEST_gitlab_backup.tar [Content-Type=application/x-tar]...
   - [1 files][ 10.0 KiB/ 10.0 KiB]
   Operation completed over 1 objects/10.0 KiB.
   gs://my-user-backup-test-storage/1234567890_1234_12_12_12_TEST_gitlab_backup.tar
   Removing gs://my-user-backup-test-storage/1234567890_1234_12_12_12_TEST_gitlab_backup.tar...
   / [1 objects]
   Operation completed over 1 objects.
   ```

 1. Also confirm in the output that the gsutil version matches the one you're reviewing for.